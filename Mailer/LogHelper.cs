﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace tripme
{
    public static class LogHelper
    {
        public static void Log(string msg)
        {
            System.IO.StreamWriter sw = System.IO.File.AppendText(string.Format("{0}\\Log_{1}.txt", Application.StartupPath, DateTime.Now.ToString("ddMMyyyy")));
            try
            {
                string logLine = System.String.Format(
                    "[{0:}] {1}.", System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), msg);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }
    }
}
