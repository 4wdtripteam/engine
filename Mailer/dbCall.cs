﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
using System.Windows.Forms;
using System.Configuration;

namespace tripme
{

    public class dbCall
    {
        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"];


        public DataSet EmailQueue_GetNotSent_ToNonMember()
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailQueue_GetNotSent_ToNonMember", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Now", DateTime.Now);

            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "emails");

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        /// <summary>
        /// Retrieve all the pending emails
        /// </summary>
        /// <returns></returns>
        public DataSet EmailQueue_GetNotSent()
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailQueue_GetNotSent", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "emails");

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        /// <summary>
        /// Set Email Sent
        /// </summary>
        /// <param name="templateid"></param>
        /// <returns></returns>
        public void EmailQueue_SetSent(int emailqueueid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailQueue_SetSent", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@EmailQueueID", emailqueueid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        /// <summary>
        /// Set Email has problem
        /// </summary>
        /// <param name="emailqueueid"></param>
        public void EmailQueue_SetProblem(int emailqueueid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailQueue_SetProblem", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@EmailQueueID", emailqueueid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        /// <summary>
        /// This is for getting email header and footer template
        /// </summary>
        /// <param name="templateid"></param>
        /// <returns></returns>
        public string EmailTemplate_GetBody(int templateid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailTemplate_GetBody", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TemplateID", templateid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
    }
}
