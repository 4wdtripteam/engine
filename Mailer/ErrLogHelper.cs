﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
namespace tripme
{
    public static class ErrLogHelper
    {
        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"];

        public static void Add(string details, string url, int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ErrLog_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Details", details);
            command.Parameters.AddWithValue("@URL", url);
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@SourceID", 0);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
    }
}
