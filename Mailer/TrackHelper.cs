﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
using System.Web.Security;
using System.Web.Configuration;

namespace tripme
{
    public static class TrackHelper
    {
        #region
        public static string MyConnection = ConfigurationManager.AppSettings["ConnectDB"];
        public static string sDomain = ConfigurationManager.AppSettings["Domain"];
        
        
        public static int Add(string trackname, string area, string state, int season, int rate, string details)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TrackName", trackname);
            command.Parameters.AddWithValue("@Area", area);
            command.Parameters.AddWithValue("@State", state);
            command.Parameters.AddWithValue("@Season", season);
            command.Parameters.AddWithValue("@Rate", rate);
            command.Parameters.AddWithValue("@Details", details);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static string GetTrackName(int trackid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_GetTrackName", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TrackID", trackid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static string GetArea(int trackid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_GetArea", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TrackID", trackid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static string GetState(int trackid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_GetState", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TrackID", trackid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static string GetDetails(int trackid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_GetDetails", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TrackID", trackid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static int CountList(string state, string area, string season, string rate, string trackname)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_CountList", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            if (state != "")
                command.Parameters.AddWithValue("@State", state);
            if (area != "")
                command.Parameters.AddWithValue("@Area", area);
            if (season != "")
                command.Parameters.AddWithValue("@Season", Convert.ToInt32(season));
            if (rate != "")
                command.Parameters.AddWithValue("@Rate", Convert.ToInt32(rate));
            if (trackname != "")
            {

                if (trackname.Length == 1)
                    command.Parameters.AddWithValue("@TrackName", trackname + "%");
                else
                    command.Parameters.AddWithValue("@TrackName", "%" + trackname + "%");
            }



            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet GetList(string sortby, int currentpage, string state, string area, string season, string rate, string trackname)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_GetList", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SortBy", sortby);
            command.Parameters.AddWithValue("@CurrentPage", currentpage);
            if (state != "")
                command.Parameters.AddWithValue("@State", state);
            if(area != "")
                command.Parameters.AddWithValue("@Area", area);
            if (season != "")
                command.Parameters.AddWithValue("@Season", Convert.ToInt32(season));
            if (rate != "")
                command.Parameters.AddWithValue("@Rate", Convert.ToInt32(rate));
            if (trackname != "")
            {

                if (trackname.Length == 1)
                    command.Parameters.AddWithValue("@TrackName", trackname + "%");
                else
                    command.Parameters.AddWithValue("@TrackName", "%" + trackname + "%");
            }



            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static DataSet GetList_Full()
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_GetList_Full", objConnection);
            command.CommandType = CommandType.StoredProcedure;

            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static DataSet GetByTrackID(int trackid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_GetByTrackID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TrackID", trackid);


            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static void SetRate(int trackid, int rate)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_SetRate", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TrackID", trackid);
            command.Parameters.AddWithValue("@Rate", rate);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void SetDetails(int trackid, string details)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_SetDetails", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TrackID", trackid);
            command.Parameters.AddWithValue("@Details", details);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void SetMapImg(int trackid, string mapimg)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_SetMapImg", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TrackID", trackid);
            command.Parameters.AddWithValue("@MapImg", mapimg);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static void IncreImp(int trackid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_IncreImp", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TrackID", trackid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void IncreClicks(int trackid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Track_IncreClicks", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TrackID", trackid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        #endregion
    }
}
