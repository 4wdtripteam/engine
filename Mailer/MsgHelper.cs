﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
namespace tripme
{
    public static class MsgHelper
    {
        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"];

        public static bool HasFilteredWord(string word)
        {
            if (word.Trim().ToLower().Contains(" bond") ||
                word.Trim().ToLower().Contains(" money") ||
                word.Trim().ToLower().Contains(" payment")
                )
                return true;
            else
            {
                return false;
            }

        }

        public static int Add(int fromuserid, int touserid, string title, string details, int replymsgid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@FromUserID", fromuserid);
            command.Parameters.AddWithValue("@ToUserID", touserid);
            command.Parameters.AddWithValue("@Title", title);
            command.Parameters.AddWithValue("@Details", details);
            command.Parameters.AddWithValue("@ReplyMsgID", replymsgid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static int CountByToUserID(int touserid, int status, int readfilter)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_CountByToUserID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ToUserID", touserid);
            command.Parameters.AddWithValue("@Status", status);

            if (readfilter == 1)
                command.Parameters.AddWithValue("@IsRead", true);
            else if (readfilter == 2)
                command.Parameters.AddWithValue("@IsRead", false);
            else { }

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static DataSet GetByUserID(int userid, int status, int readfilter, string sortby, int rpp, int pg)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_GetByUserID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Status", status);

            if (readfilter == 1)
                command.Parameters.AddWithValue("@IsRead", true);
            else if (readfilter == 2)
                command.Parameters.AddWithValue("@IsRead", false);
            else { }

            command.Parameters.AddWithValue("@SortBy", sortby);
            command.Parameters.AddWithValue("@Rpp", rpp);
            command.Parameters.AddWithValue("@CurrentPage", pg);

            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static DataSet GetByToUserID(int touserid, int status, int readfilter, string sortby, int rpp, int pg)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_GetByToUserID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ToUserID", touserid);
            command.Parameters.AddWithValue("@Status", status);

            if(readfilter == 1)
                command.Parameters.AddWithValue("@IsRead", true);
            else if (readfilter == 2)
                command.Parameters.AddWithValue("@IsRead", false);
            else { }

            command.Parameters.AddWithValue("@SortBy", sortby);
            command.Parameters.AddWithValue("@Rpp", rpp);
            command.Parameters.AddWithValue("@CurrentPage", pg);

            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int CountByUserID(int userid, int status, int readfilter)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_CountByUserID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Status", status);

            if (readfilter == 1)
                command.Parameters.AddWithValue("@IsRead", true);
            else if (readfilter == 2)
                command.Parameters.AddWithValue("@IsRead", false);
            else { }

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int CountByFromUserID(int touserid, int status, int readfilter)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_CountByFromUserID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@FromUserID", touserid);
            command.Parameters.AddWithValue("@Status", status);

            if (readfilter == 1)
                command.Parameters.AddWithValue("@IsRead", true);
            else if (readfilter == 2)
                command.Parameters.AddWithValue("@IsRead", false);
            else { }

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet GetAllReply(int msgid, int userid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_GetAllReply", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@MsgID", msgid);
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet GetLatestReply(int msgid, int userid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_GetLatestReply", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@MsgID", msgid);
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet GetByFromUserID(int fromuserid, int status, int readfilter, string sortby, int rpp, int pg)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_GetByFromUserID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@FromUserID", fromuserid);
            command.Parameters.AddWithValue("@Status", status);

            if (readfilter == 1)
                command.Parameters.AddWithValue("@IsRead", true);
            else if (readfilter == 2)
                command.Parameters.AddWithValue("@IsRead", false);
            else { }

            command.Parameters.AddWithValue("@SortBy", sortby);
            command.Parameters.AddWithValue("@Rpp", rpp);
            command.Parameters.AddWithValue("@CurrentPage", pg);

            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet GetByMsgID(int msgid, int userid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_GetByMsgID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@MsgID", msgid);
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static string GetTitle(int msgid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_GetTitle", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@MsgID", msgid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }



        public static void Delete(int msgid, int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_Delete", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@MsgID", msgid);
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static void SetIsRead(int msgid, int touserid, bool isread)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_SetIsRead", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@MsgID", msgid);
            command.Parameters.AddWithValue("@ToUserID", touserid);
            command.Parameters.AddWithValue("@IsRead", isread);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void SetStatus(int msgid, int userid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_SetStatus", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@MsgID", msgid);
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void SetFromDel(int msgid, bool fromdel, int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_SetFromDel", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@MsgID", msgid);
            command.Parameters.AddWithValue("@FromDel", fromdel);
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void SetToDel(int msgid, bool todel, int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_SetToDel", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@MsgID", msgid);
            command.Parameters.AddWithValue("@ToDel", todel);
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        
        public static int GetFromUserID(int msgid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_GetFromUserID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@MsgID", msgid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
        public static int GetToUserID(int msgid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_GetToUserID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@MsgID", msgid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
        public static int CountReply(int userid, int msgid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_CountReply", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@MsgID", msgid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
        public static int CountUnreadReply(int userid, int msgid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_CountUnreadReply", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@MsgID", msgid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
        public static int CountAllUnreadReply(int userid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_CountAllUnreadReply", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
        public static int CountAllUnreadMsg(int userid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_CountAllUnreadMsg", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }

        public static bool IsValid(int touserid, int msgid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("_IsValid", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ToUserID", touserid);
            command.Parameters.AddWithValue("@MsgID", msgid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                return Convert.ToBoolean(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
        public static bool HasReply(int touserid, int msgid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Msg_HasReply", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ToUserID", touserid);
            command.Parameters.AddWithValue("@MsgID", msgid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                return Convert.ToBoolean(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }

    }
}
