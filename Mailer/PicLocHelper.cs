﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
namespace tripme
{
    public static class PicLocHelper
    {
        #region
        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"];

        public static string GetPicLocPath(int piclocid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("PicLoc_GetPicLocPath", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@PicLocID", piclocid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        #endregion
    }
}
