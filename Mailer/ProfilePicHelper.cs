﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;

namespace tripme
{
    public static class ProfilePicHelper
    {

        #region
        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"];
        public static String sDomain = System.Configuration.ConfigurationManager.AppSettings["Domain"];
        public static String sSecureDomain = System.Configuration.ConfigurationManager.AppSettings["SecureDomain"];

        public static int Add(int userid, byte[] imagedata)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ProfilePic_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@ImageData", (object)imagedata);

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        #endregion

    }
}
