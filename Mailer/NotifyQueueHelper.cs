﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
namespace tripme
{
    public static class NotifyQueueHelper
    {

        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"];
        public static int Add(int whatid, string whatvalue, int notifytypeid, bool issenddirect, bool isprivatemsg)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("NotifyQueue_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@WhatID", whatid);
            command.Parameters.AddWithValue("@WhatValue", whatvalue);
            command.Parameters.AddWithValue("@NotifyTypeID", notifytypeid);
            command.Parameters.AddWithValue("@IsSendDirect", issenddirect);
            command.Parameters.AddWithValue("@IsPrivateMsg", isprivatemsg);

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet GetPending()
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("NotifyQueue_GetPending", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void Set(int whatid, int notifytypeid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("NotifyQueue_SetStatus", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@WhatID", whatid);
            command.Parameters.AddWithValue("@NotifyTypeID", notifytypeid);
            command.Parameters.AddWithValue("@Status", status);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void SetStatus(int whatid, int notifytypeid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("NotifyQueue_SetStatus", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@WhatID", whatid);
            command.Parameters.AddWithValue("@NotifyTypeID", notifytypeid);
            command.Parameters.AddWithValue("@Status", status);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

    }
}
