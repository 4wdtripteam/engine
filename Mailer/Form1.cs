﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Timers;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Amazon.SimpleEmail.Model;
using System.IO;
using Amazon.S3;
using Amazon.S3.Transfer;

namespace tripme
{
    public partial class Form1 : Form
    {
        public String sSecureDomain = System.Configuration.ConfigurationManager.AppSettings["SecureDomain"];
        public String sDomain = System.Configuration.ConfigurationManager.AppSettings["Domain"];
        private System.Timers.Timer timer;
        dbCall mydb = new dbCall();
        public string emailBodyHead = "";
        public string emailBodyFoot = "";
        public string emailBodyFoot_nonMember = "";

        public string streetname = "";
        public string streettypename = "";
        public int streettypeid = 0;
        public string address = "";



        private void Form1_Load(object sender, EventArgs e)
        { }

        public Form1()
        {
            timer = new System.Timers.Timer();
            timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            timer.Interval = 15000;
            timer.Enabled = true;

            InitializeComponent();

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            //first time run
            lblEngineStatus.Text = "Start processing...";
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            timer.Start();


        }
        void OnTimedEvent(object source, ElapsedEventArgs e)
        {
             timer.Enabled = false;

            ProcessEmail();
            ProcessNotify();
            TripReminder_Tomorrow();
            TripReminder_NextWeek();
            TripReport();

            timer.Enabled = true;
        }





        void TripReport()
        {
            try
            {
                int templateid = 73;

                DataSet ds = new DataSet();
                ds = TripHelper.JustFinishedTrip(); // 3 days after the trip
                if (ds != null)
                {
                    int tripid = 0;
                    int fromuserid = 0;
                    string title = "";

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        tripid = Convert.ToInt32(dr["TripID"].ToString());
                        fromuserid = Convert.ToInt32(dr["UserID"].ToString());
                        title = TripHelper.GetTitle(tripid);

                        string subject = EmailTemplateHelper.GetSubject(templateid);
                        subject = subject.Replace("@@Trip", title);


                        string body = EmailTemplateHelper.GetBody(templateid);
                        body = body.Replace("@@Location", "<strong>" + TripHelper.GetLocation(tripid) + "</strong>");
                        body = body.Replace("@@TripReport",
                                            "<a href='" + sDomain + "/offroad/trip/report.aspx?tripid=" + tripid +
                                            "'>Post a trip report and comment</a>");
                        body = body.Replace("@@PostedBy",
                                            "<a href='" + sDomain + "/offroad/member/rating.aspx?u=" + fromuserid +
                                            "'>Leave a feedback to Trip Leader</a>");


                        DataSet ds2 = new DataSet();
                        ds2 = TripUserHelper.GetByTripID(tripid);


                        string bodytemplate_going = "";

                        // send to each member
                        if (ds2 != null)
                        {
                            int userid = 0;
                            int status = 0;
                            foreach (DataRow dr2 in ds2.Tables[0].Rows)
                            {
                                userid = Convert.ToInt32(dr2["UserID"].ToString());
                                status = Convert.ToInt32(dr2["Status"].ToString());

                                if (userid != fromuserid) 
                                {
                                    if (status == 1)
                                    {
                                        bodytemplate_going = body;
                                        bodytemplate_going = bodytemplate_going.Replace("@@Firstname", UserHelper.GetFirstname(userid));
                                        EmailQueueHelper.Add(userid, subject, bodytemplate_going, "", DateTime.Now);
                                    }
                                }




                            }
                        }

                        TripDetailHelper.Add(tripid, 15, 0, "");
                    }
                }
            }
            catch (Exception ex)
            {
                UpdateStatus(ex.Message);
                LogHelper.Log("ProcessEmail - " + ex.ToString());
            }
            finally
            {
                UpdateStatus("Checking...");
                LogHelper.Log("Checking..." + System.DateTime.Now);
            }


        }


        void TripReminder_NextWeek()
        {
            try
            {
                int templateid = 71; 

                DataSet ds = new DataSet();
                ds = TripHelper.UpComingTrip_NextWeek(); // always send the latest email first
                if (ds != null)
                {
                    int tripid = 0;
                    int fromuserid = 0;
                    string title = "";

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        tripid = Convert.ToInt32(dr["TripID"].ToString());
                        fromuserid = Convert.ToInt32(dr["UserID"].ToString());
                        title = TripHelper.GetTitle(tripid);

                        string subject = EmailTemplateHelper.GetSubject(templateid);
                        subject = subject.Replace("@@Trip", title);


                        string body = EmailTemplateHelper.GetBody(templateid);
                        body = body.Replace("@@Location", "<strong>" + TripHelper.GetLocation(tripid) + "</strong>");
                        body = body.Replace("@@Trip", "<a href='" + sDomain + "/trip.aspx?tripid=" +
                                            tripid + "' target='_blank' >" + title + "</a>");
                        body = body.Replace("@@StartDate", "Date: <strong>" + TripHelper.GetStartDate(tripid).ToString("MMM dd, yyyy") + "</strong>");

                        string meetat = "";
                        DateTime starttime = TripHelper.GetStartDate(tripid);
                        if (starttime.Hour < 12)
                        {
                            meetat = starttime.ToString("hh:mm") + " AM";
                        }
                        else
                        {
                            meetat = starttime.ToString("hh:mm") + " PM";
                        }

                        body = body.Replace("@@Meeting", "Meeting: <strong>" + meetat + "</strong> at <strong>" + TripHelper.GetMeetingPoint(tripid) + "</strong>");
                        body = body.Replace("@@PostedBy", "Organiser: <a href='" + sDomain + "/member/" + fromuserid + "'>" +
                                           UserHelper.GetFirstname(fromuserid) + " (" +
                                           UserHelper.GetUsernameByUserID(fromuserid) + ")" + "</a> - " + UserDetailHelper.GetTextByUserDetailTypeID(fromuserid, 18));


                        DataSet ds2 = new DataSet();
                        ds2 = TripUserHelper.GetByTripID(tripid);


                        string bodytemplate_going = "";

                        // send to each member
                        if (ds2 != null)
                        {
                            int userid = 0;
                            int status = 0;
                            foreach (DataRow dr2 in ds2.Tables[0].Rows)
                            {
                                userid = Convert.ToInt32(dr2["UserID"].ToString());
                                status = Convert.ToInt32(dr2["Status"].ToString());

                                if (userid == fromuserid) // send a copy to organiser
                                {
                                    bodytemplate_going = body;
                                    bodytemplate_going = bodytemplate_going.Replace("@@Firstname", UserHelper.GetFirstname(userid));
                                    EmailQueueHelper.Add(userid, subject, bodytemplate_going, "", DateTime.Now);

                                }
                                else
                                {

                                    if (status == 1)
                                    {
                                        bodytemplate_going = body;
                                        bodytemplate_going = bodytemplate_going.Replace("@@Firstname", UserHelper.GetFirstname(userid));
                                        EmailQueueHelper.Add(userid, subject, bodytemplate_going, "", DateTime.Now);
                                    }
                                }




                            }

                            // send copy to 4WD Trip Team
                            bodytemplate_going = bodytemplate_going.Replace("@@Firstname", "");
                            EmailQueueHelper.Add(1, "COPY - " + subject, bodytemplate_going, "", DateTime.Now);

                        }

                        TripDetailHelper.Add(tripid, 14, 0, "");
                    }
                }
            }
            catch (Exception ex)
            {
                UpdateStatus(ex.Message);
                LogHelper.Log("ProcessEmail - " + ex.ToString());
            }
            finally
            {
                UpdateStatus("Checking...");
                LogHelper.Log("Checking..." + System.DateTime.Now);
            }


        }
        void TripReminder_Tomorrow()
        {
            try
            {
                int templateid_going = 58; // who are going
                int templateid_interested = 62; // who are interested
                int templateid_safetychecklist = 77;

                DataSet ds = new DataSet();
                ds = TripHelper.UpComingTrip(24); // always send the latest email first
                if (ds != null)
                {
                    int tripid = 0;
                    int fromuserid = 0;
                    string title = "";

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        tripid = Convert.ToInt32(dr["TripID"].ToString());
                        fromuserid = Convert.ToInt32(dr["UserID"].ToString());
                        title = TripHelper.GetTitle(tripid);

                        // this is the email template for people who are going
                        string subject = EmailTemplateHelper.GetSubject(templateid_going);
                        subject = subject.Replace("@@Trip", title);


                        string body = EmailTemplateHelper.GetBody(templateid_going);
                        body = body.Replace("@@Location", "<strong>" + TripHelper.GetLocation(tripid) + "</strong>");
                        body = body.Replace("@@Trip", "<a href='" + sDomain + "/trip.aspx?tripid=" +
                                            tripid + "' target='_blank' >" + title + "</a>");
                        body = body.Replace("@@StartDate", "Date: <strong>" + TripHelper.GetStartDate(tripid).ToString("MMM dd, yyyy") + "</strong>");

                        string meetat = "";
                        DateTime starttime = TripHelper.GetStartDate(tripid);
                        if(starttime.Hour < 12)
                        {
                            meetat = starttime.ToString("hh:mm") + " AM";
                        }
                        else
                        {
                            meetat = starttime.ToString("hh:mm") + " PM";
                        }

                        body = body.Replace("@@Meeting", "Meeting: <strong>" + meetat + "</strong> at <strong>" + TripHelper.GetMeetingPoint(tripid) + "</strong>");
                        body = body.Replace("@@PostedBy", "Organiser: <a href='" + sDomain + "/member/" + fromuserid + "'>" +
                                           UserHelper.GetFirstname(fromuserid) + " (" +
                                           UserHelper.GetUsernameByUserID(fromuserid) + ")" + "</a> - " + UserDetailHelper.GetTextByUserDetailTypeID(fromuserid, 18));



                        // this is the email for people who are interested
                        string subject2 = EmailTemplateHelper.GetSubject(templateid_interested);
                        subject2 = subject2.Replace("@@Trip", title);
                        string body2 = EmailTemplateHelper.GetBody(templateid_interested);

                        body2 = body2.Replace("@@Location", "<strong>" + TripHelper.GetLocation(tripid) + "</strong>");
                        body2 = body2.Replace("@@Trip", "<a href='" + sDomain + "/trip.aspx?tripid=" +
                                            tripid + "' target='_blank' >" + title + "</a>");
                        body2 = body2.Replace("@@StartDate", "Date: <strong>" + TripHelper.GetStartDate(tripid).ToString("MMM dd, yyyy") + "</strong>");
                        body2 = body2.Replace("@@Meeting", "Meeting: <strong>" + TripHelper.GetStartDate(tripid).ToString("hh:mm") + "</strong> at <strong>" + TripHelper.GetMeetingPoint(tripid) + "</strong>");
                        body2 = body2.Replace("@@PostedBy", "Organiser: <a href='" + sDomain + "/member/" + fromuserid + "'>" +
                                           UserHelper.GetFirstname(fromuserid) + " (" +
                                           UserHelper.GetUsernameByUserID(fromuserid) + ")" + "</a> - " + UserDetailHelper.GetTextByUserDetailTypeID(fromuserid, 18));



                        DataSet ds2 = new DataSet();
                        ds2 = TripUserHelper.GetByTripID(tripid);

                        string members_forParticipants = "<ol>";
                        string members_forOrganiser = "<ol>";

                        if (ds2 != null)
                        {
                            int userid = 0;
                            string firstname = "";
                            string username = "";
                            int status = 0;

                            foreach (DataRow dr2 in ds2.Tables[0].Rows)
                            {
                                userid = 0;
                                firstname = "";
                                username = "";
                                userid = Convert.ToInt32(dr2["UserID"].ToString());
                                status = Convert.ToInt32(dr2["Status"].ToString());
                                firstname = UserHelper.GetFirstname(userid);
                                username = UserHelper.GetUsernameByUserID(userid);
                                if (status == 0) // if interested, do not include them in the list
                                continue;




                                string emergency = "";
                                // check if any emergency contact
                                if (UserDetailHelper.Has(userid, 70))
                                {
                                    string emergency_name = UserDetailHelper.GetTextByUserDetailTypeID(userid, 70);
                                    string emergency_phone = UserDetailHelper.GetTextByUserDetailTypeID(userid, 72);
                                    string emergency_relationship = UserDetailHelper.GetTextByUserDetailTypeID(userid, 73);
                                    emergency = "<br>Emergency contact: " + emergency_name + " (" + emergency_relationship + ") - " + emergency_phone;

                                }
                                members_forOrganiser += "<li><a href='" + sDomain + "/member/" + userid + "'>" +
                                           firstname + " (" +
                                           username + ")" + "</a> - " + UserDetailHelper.GetTextByUserDetailTypeID(userid, 18) + emergency + "</li>";

                                if(userid == fromuserid)
                                {
                                    members_forParticipants += "<li>(Organiser) <a href='" + sDomain + "/member/" + userid + "'>" +
                                               firstname + " (" +
                                               username + ")" + "</a> - " + UserDetailHelper.GetTextByUserDetailTypeID(userid, 18) + "</li>";

                                }
                                else
                                {

                                    members_forParticipants += "<li><a href='" + sDomain + "/member/" + userid + "'>" +
                                               firstname + " (" +
                                               username + ")" + "</a></li>";

                                }

                            }
                        }
                        members_forParticipants += "</ol>";
                        members_forOrganiser += "</ol>";

                        string bodytemplate_going = "";
                        string bodytemplate_interested = "";

                        // send to each member
                        if (ds2 != null)
                        {
                            int userid = 0;
                            int status = 0;
                            // check if the trip is already full, if yes, don't send to Interested people
                            int max = TripDetailHelper.GetByTripDetailTypeID(tripid, 3);
                            int going = TripUserHelper.CountByStatus(tripid, 1);
                            bool isfull = false;
                            if (going >= max && max > 0)
                                isfull = true;

                            string checkinUrl = "";


                            foreach (DataRow dr2 in ds2.Tables[0].Rows)
                            {
                                userid = Convert.ToInt32(dr2["UserID"].ToString());
                                status = Convert.ToInt32(dr2["Status"].ToString());

                               
                                DateTime checkin = DateTime.Parse(dr2["CheckIn"].ToString());
                                if (checkin.Year > 2000)
                                {
                                    subject = "Trip Reminder - " + title;
                                    checkinUrl = "";

                                }
                                else
                                {

                                    subject = "Check-In Required - " + title;
                                    checkinUrl = "<div style='background:#51a351; border-radius:3px 3px 3px 3px; border:1px solid #155815; font-family:arial; font-size:16px; font-weight:bold; width:200px'>" +
                                    "<a href='" + sDomain + "/offroad/account/trip/checkin.aspx?tripid=" + tripid + "&u=" + userid + "'" +
                                    " style='padding:5px; font-size: 16px; font-weight: bold; text-decoration: none; color: #ffffff' target='_blank'>Click here to CHECK-IN</a></div>";

                                }

                                if (userid == fromuserid && going > 1) // send a copy to organiser
                                {
                                    bodytemplate_going = body;
                                    bodytemplate_going = bodytemplate_going.Replace("@@Firstname", UserHelper.GetFirstname(userid));
                                    bodytemplate_going = bodytemplate_going.Replace("@@Members", members_forOrganiser);
                                    bodytemplate_going = bodytemplate_going.Replace("@@Checkin", checkinUrl);

                                    EmailQueueHelper.Add(userid, subject, bodytemplate_going, "", DateTime.Now);
                                    
                                    // check if any emergency contact
                                    if(UserDetailHelper.Has(userid, 71))
                                    {
                                        string emergency_email = UserDetailHelper.GetTextByUserDetailTypeID(userid, 71);
                                        bodytemplate_going =
                                            "<p><strong>**** This is a copy of the trip detail for emergency contact (YOU) ****</strong></p>" + bodytemplate_going;
                                        EmailQueueHelper.Add(userid, "FYI - Trip Reminder - " + title, bodytemplate_going, emergency_email, DateTime.Now);
                                    }
                                }
                                else
                                {


                                    if (status == 1 && going > 1)
                                    {
                                        bodytemplate_going = body;
                                        bodytemplate_going = bodytemplate_going.Replace("@@Firstname", UserHelper.GetFirstname(userid));
                                        bodytemplate_going = bodytemplate_going.Replace("@@Members", members_forParticipants);
                                        bodytemplate_going = bodytemplate_going.Replace("@@Checkin", checkinUrl);  
                                        
                                        EmailQueueHelper.Add(userid, subject, bodytemplate_going, "", DateTime.Now);

                                        // check if any emergency contact
                                        if (UserDetailHelper.Has(userid, 71))
                                        {
                                            string emergency_email = UserDetailHelper.GetTextByUserDetailTypeID(userid, 71);
                                            bodytemplate_going =
                                                "<p><strong>**** This is a copy of the trip detail for emergency contact (YOU) ****</strong></p>" + bodytemplate_going;
                                            EmailQueueHelper.Add(userid, "FYI - Trip Reminder - " + title, bodytemplate_going, emergency_email, DateTime.Now);
                                        }


                                        // this is the email for people who are interested
                                        string subject_checklist = EmailTemplateHelper.GetSubject(templateid_safetychecklist);
                                        string body_checklist = EmailTemplateHelper.GetBody(templateid_safetychecklist);
                                        body_checklist = body_checklist.Replace("@@Firstname", UserHelper.GetFirstname(userid));

                                        EmailQueueHelper.Add(userid, subject_checklist, body_checklist, "", DateTime.Now);

                                    }
                                    else if (status == 0 && !isfull)
                                    {
                                        bodytemplate_interested = body2;
                                        bodytemplate_interested = bodytemplate_interested.Replace("@@Firstname", UserHelper.GetFirstname(userid));
                                        bodytemplate_interested = bodytemplate_interested.Replace("@@Members", members_forParticipants);
                                        bodytemplate_interested = bodytemplate_interested.Replace("@@Going", "<a href='" + sDomain + "/trip.aspx?tripid=" + tripid + "&status=1&u=" + userid + "' target='_blank' >YES, I AM GOING!</a>");

                                        EmailQueueHelper.Add(userid, subject2, bodytemplate_interested, "", DateTime.Now);

                                    }
                                }




                            }

                            // send copy to 4WD Trip Team
                            bodytemplate_going = bodytemplate_going.Replace("@@Firstname", "");
                            bodytemplate_going = bodytemplate_going.Replace("@@Members", members_forOrganiser);
                            EmailQueueHelper.Add(1, "4WDTRIP Team COPY - " + subject, bodytemplate_going, "", DateTime.Now);                                       

                        }

                        TripDetailHelper.Add(tripid, 5, 0, "");
                    }
                }
            }
            catch (Exception ex)
            {
                UpdateStatus(ex.Message);
                LogHelper.Log("ProcessEmail - " + ex.ToString());
            }
            finally
            {
                UpdateStatus("Checking...");
                LogHelper.Log("Checking..." + System.DateTime.Now);
            }


        }
        void ProcessEmail()
        {
            try
            {
                if (KeyHelper.GetValue("Send Email") == "0")
                    return;

                string SendBySendGrid = "0";
                if (KeyHelper.GetValue("SendBySendGrid") == "1")
                    SendBySendGrid = KeyHelper.GetValue("SendBySendGrid");

                DataSet ds = new DataSet();
                ds = mydb.EmailQueue_GetNotSent(); // always send the latest email first
                // EmailQueueID, Firstname, Username, UserID, Email, EmailSubject, EmailBody, AltEmail, FromUserID
                if (ds != null)
                {

                    bool SentOk = false;
                    int emailqueueid = 0;
                    string firstname = "";
                    string username = "";
                    int userid = 0;
                    string email = "";
                    string emailsubject = "";
                    string emailbody = "";
                    string altemail = "";
                    int fromuserid = 0;
                    int mailoutid = 0;

                    int countsent = 0;

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        SentOk = false;
                        emailqueueid = 0;
                        firstname = "";
                        username = "";
                        userid = 0;
                        fromuserid = 0;
                        email = "";
                        emailsubject = "";
                        emailbody = "";
                        altemail = "";

                        emailqueueid = Convert.ToInt32(dr["EmailQueueID"].ToString());
                        firstname = dr["Firstname"].ToString();
                        username = dr["Username"].ToString();
                        userid = Convert.ToInt32(dr["UserID"].ToString());
                        email = dr["Email"].ToString();
                        emailsubject = dr["EmailSubject"].ToString();
                        emailbody = dr["EmailBody"].ToString();
                        altemail = dr["AltEmail"].ToString();
                        fromuserid = Convert.ToInt32(dr["FromUserID"].ToString());
                        mailoutid = Convert.ToInt32(dr["MailOutID"].ToString());

                        emailBodyHead = mydb.EmailTemplate_GetBody(1);
                        emailBodyFoot = mydb.EmailTemplate_GetBody(2);

                        emailBodyHead = emailBodyHead.Replace("@@Subject", emailsubject);
                        emailBodyHead = emailBodyHead.Replace("@@Date", DateTime.Now.ToString("MMM dd, yyyy"));

                        if (altemail != null && altemail != "")
                            email = altemail;
                        
                        SentOk = SendGrid(userid, emailqueueid, emailsubject, emailbody, fromuserid, email);

                        if (SentOk)
                        {
                            mydb.EmailQueue_SetSent(emailqueueid); // Set Sent (Status = 1)
                            countsent += 1; // keep track on how many have been sent in this loop
                        }
                        else
                        {
                            mydb.EmailQueue_SetProblem(emailqueueid); //Set problem (Status = 2)
                            // Notify Us by email for any email problem
                            NotifyAdmin(username, userid, email, emailsubject, emailbody);
                        }

                        // send in 100x emails batches when sending a lot of emails
                        if (countsent >= 100)
                            break;
                    }
                }



            }
            catch (Exception ex)
            {
                UpdateStatus(ex.Message);
                LogHelper.Log("ProcessEmail - " + ex.ToString());
            }
            finally
            {
                UpdateStatus("Checking...");
                LogHelper.Log("Checking..." + System.DateTime.Now);
            }
        }

        void ProcessNotify()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = NotifyQueueHelper.GetPending();
                int c = ds.Tables[0].Rows.Count;

                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    emailBodyHead = EmailTemplateHelper.GetBody(1);
                    emailBodyFoot = EmailTemplateHelper.GetBody(2);
                    emailBodyFoot_nonMember = EmailTemplateHelper.GetBody(25);

                    //DishID, UserID, Title, Vote, PicID1, DishStatsValue as 'Viewed' 
                    int whatid = 0;
                    string whatvalue = "";
                    int notifytypeid = 0;
                    bool issenddirect = false;
                    bool isprivatemsg = false;

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        whatid = Convert.ToInt32(dr["WhatID"].ToString());
                        whatvalue = dr["WhatValue"].ToString();
                        notifytypeid = Convert.ToInt32(dr["NotifyTypeID"].ToString());
                        issenddirect = Convert.ToBoolean(dr["IsSendDirect"].ToString());
                        isprivatemsg = Convert.ToBoolean(dr["IsPrivateMsg"].ToString());
                        if (notifytypeid == 1) // send trip alert immediately
                        {
                            NotifyNewTrip(whatid, 1);
                        }
                        else if (notifytypeid == 3)
                        {
                            NewPost_SameState(whatid); // same state as poster
                        }
                        else if (notifytypeid == 5)
                        {
                            NewPost(whatid); // all state
                        }
                        NotifyQueueHelper.SetStatus(whatid, notifytypeid, 1);

                    }

                }

            }
            catch (Exception ex)
            {
                UpdateStatus(ex.Message);
                LogHelper.Log(ex.ToString());
            }
            finally
            {
                UpdateStatus("Checking...");
                LogHelper.Log("Checking..." + System.DateTime.Now);
            }
        }
        void NewPost(int forumpostid)
        {
            int userid = 0;
            try
            {


                string title = ForumPostHelper.GetTitle(forumpostid);
                string content = ForumPostHelper.GetMessage(forumpostid);
                // add photo domain

                int postuserid = ForumPostHelper.GetUserID(forumpostid);
                string authorFirstname = UserHelper.GetFirstname(postuserid);
                string authorUsername = UserHelper.GetUsernameByUserID(postuserid);
                string author = "<a href='" + sDomain + "/member/" + postuserid + "'>" + authorFirstname + " (" + authorUsername + ")</a>";

                string state = UserHelper.GetState(postuserid);
                string reply = "<a href='" + sDomain + "/offroad/social/forum/read.aspx?originalpostid=" + forumpostid +
                               "'>Click here to view and reply</a>";

                string titleURL = "<a href='" + sDomain + "/offroad/social/forum/read.aspx?originalpostid=" + forumpostid + "'>" + title + "</a>";

                int templateid = 69;


                //Retreive members           
                DataSet ds = UserHelper.NotifyAllUser();

                if (ds.Tables.Count >= 1 && ds.Tables[0].Rows.Count > 0)
                {


                    string subject = EmailTemplateHelper.GetSubject(templateid);
                    subject = subject.Replace("@@Title", title);

                    string bodytemplate = EmailTemplateHelper.GetBody(templateid);
                    string body = "";
                    string username = "";
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        userid = Convert.ToInt32(dr["UserID"].ToString());

                        if (postuserid == userid)
                            continue;

                        username = dr["Username"].ToString();

                        body = bodytemplate;
                        body = body.Replace("@@Firstname", UserHelper.GetFirstname(userid));
                        body = body.Replace("@@Author", author);
                        body = body.Replace("@@TitleURL", titleURL);
                        body = body.Replace("@@Details", content);
                        body = body.Replace("@@Reply", reply);

                        DateTime senddate = DateTime.Now;

                        int forumid = ForumPostHelper.GetForumID(forumpostid);
                        string subjectprefix = "New Discussion - ";
                        if (forumid == 1)
                            subjectprefix = "";


                        EmailQueueHelper.Add(userid, subjectprefix + subject + " (" + username + ")", body, "", senddate);

                    }
                }

            }
            catch (Exception ex)
            {
                UpdateStatus(ex.Message);
                LogHelper.Log(ex.ToString());
                LogHelper.Log("FAIL - UserID:" + userid + ") ... " + System.DateTime.Now);

            }
            finally
            {
                UpdateStatus("Checking...");
            }



        }
        void NewPost_SameState(int forumpostid)
        {
            int userid = 0;
            try
            {


                string title = ForumPostHelper.GetTitle(forumpostid);
                string content = ForumPostHelper.GetMessage(forumpostid);

                int postuserid = ForumPostHelper.GetUserID(forumpostid);
                string authorFirstname = UserHelper.GetFirstname(postuserid);
                string authorUsername = UserHelper.GetUsernameByUserID(postuserid);
                string author = "<a href='" + sDomain + "/member/" + postuserid + "'>" + authorFirstname + " (" + authorUsername + ")</a>";

                string state = UserHelper.GetState(postuserid);
                string reply = "<a href='" + sDomain + "/offroad/social/forum/read.aspx?originalpostid=" + forumpostid +
                               "'>Click here to view and reply</a>";
                string titleURL = "<a href='" + sDomain + "/offroad/social/forum/read.aspx?originalpostid=" + forumpostid + "'>" + title + "</a>";

                int templateid = 69;


                //Retreive members           
                DataSet ds = UserHelper.NotifyAllUser_ByState(state);

                if (ds.Tables.Count >= 1 && ds.Tables[0].Rows.Count > 0)
                {


                    string subject = EmailTemplateHelper.GetSubject(templateid);
                    subject = subject.Replace("@@Title", title);

                    string bodytemplate = EmailTemplateHelper.GetBody(templateid);
                    string body = "";
                    string username = "";
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        userid = Convert.ToInt32(dr["UserID"].ToString());

                        if (postuserid == userid)
                            continue;

                        username = dr["Username"].ToString();

                        body = bodytemplate;
                        body = body.Replace("@@Firstname", UserHelper.GetFirstname(userid));
                        body = body.Replace("@@Author", author);
                        body = body.Replace("@@TitleURL", titleURL);
                        body = body.Replace("@@Details", content);
                        body = body.Replace("@@Reply", reply);

                        DateTime senddate = DateTime.Now;

                        int forumid = ForumPostHelper.GetForumID(forumpostid);
                        string subjectprefix = "New Discussion - ";
                        if (forumid == 1)
                            subjectprefix = "";


                        EmailQueueHelper.Add(userid, subjectprefix + subject + " (" + username + ")", body, "", senddate);

                    }
                }

            }
            catch (Exception ex)
            {
                UpdateStatus(ex.Message);
                LogHelper.Log(ex.ToString());
                LogHelper.Log("FAIL - UserID:" + userid + ") ... " + System.DateTime.Now);

            }
            finally
            {
                UpdateStatus("Checking...");
            }



        }

        void NotifyNewTrip(int tripid, int alerttype)
        {
            int userid = 0;
            try
            {

                string state = TripHelper.GetState(tripid);

                int templateid = 8;


                //Retreive members       
                DataSet ds;
                if (TripDetailHelper.Has(tripid, 17))
                    ds = TripHelper.NotifyAllUser();
                else
                {
                    ds = TripHelper.NotifyUser(state);
                }
                 

                if (ds.Tables.Count >= 1 && ds.Tables[0].Rows.Count > 0)
                {
                    string title = TripHelper.GetTitle(tripid);


                    DateTime dtFrom = TripHelper.GetStartDate(tripid);
                    DateTime dtTo = TripHelper.GetEndDate(tripid);
                    string when = "";
                    if (dtFrom.Date == dtTo.Date)
                    {
                        when = dtFrom.ToString("dd-MMM-yyyy");
                    }
                    else
                    {
                        when = "From " + dtFrom.ToString("dd-MMM-yyyy") + " to " + dtTo.ToString("dd-MMM-yyyy");
                    }


                    string subject = EmailTemplateHelper.GetSubject(templateid);
                    subject = subject.Replace("@@Trip", title);

                    string bodytemplate = EmailTemplateHelper.GetBody(templateid);
                    string body = "";

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        userid = Convert.ToInt32(dr["UserID"].ToString());

                        body = bodytemplate;
                        body = body.Replace("@@Firstname", UserHelper.GetFirstname(userid));
                        body = body.Replace("@@URL",
                                            "<a href='" + sDomain + "/trip.aspx?tripid=" +
                                            tripid + "' target='_blank' >" + title + "</a>");
                        body = body.Replace("@@When", when);
                        body = body.Replace("@@Location", TripHelper.GetLocation(tripid));
                        body = body.Replace("@@View", "<a href='" + sDomain + "/trip.aspx?tripid=" +
                                            tripid + "' target='_blank' >View Trip Details</a>");


                        DateTime senddate = DateTime.Now;
                        DateTime startdate = TripHelper.GetStartDate(tripid);
                        TimeSpan whentostart = startdate.Subtract(DateTime.Now);

                        if (TripUserHelper.CountByUserID(userid) > 0 && 
                            whentostart.Days > 1 && //at least one day before it starts
                            !TripDetailHelper.Has(tripid, 19)) //if it's not set to send immediately
                        {
                            senddate = DateTime.Now.AddHours(6); // delay 6 hours to send to user who have already joined trip
                        }

                        EmailQueueHelper.Add(userid, subject + " (U" + userid + ")", body, "", senddate);
                        LogHelper.Log("Add to Queue (Trip " + tripid + ") - UserID:" + userid + ") ... " +
                                        System.DateTime.Now);

                    }
                }

            }
            catch (Exception ex)
            {
                UpdateStatus(ex.Message);
                LogHelper.Log(ex.ToString());
                LogHelper.Log("FAIL - UserID:" + userid + ") ... " + System.DateTime.Now);

            }
            finally
            {
                UpdateStatus("Checking...");
            }



        }

        bool SendGrid(int userid, int emailqueueid, string subject, string body, int fromuserid, string toemail)
        {
            // add domain to local image (uploaded directly to our site) or they can't view on mobile

            // make sure email is valid before sending
            if (!Regex.IsMatch(toemail, ("^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")))
            {
                mydb.EmailQueue_SetProblem(emailqueueid); //Set problem (Status = 2)
                return false;
            }

            LogHelper.Log("SendGrid - " + toemail + " (EmailID:" + emailqueueid + ") ... " + System.DateTime.Now);

            string emailheader = EmailTemplateHelper.GetBody(1);
            string emailfooter = EmailTemplateHelper.GetBody(2);

            emailheader = emailheader.Replace("@@Subject", subject);
            body = emailheader + " " + body;
            body = body + " " + emailfooter;

            try
            {
                MailMessage Message = new MailMessage();
                Message.From = new MailAddress("team@4wdTrip.com.au", "4WDTRIP.com.au");
                Message.Headers.Add("X-Organization", "4WDTRIP.com.au");
                Message.Subject = System.Web.HttpUtility.HtmlDecode(subject);
                Message.Priority = MailPriority.High;
                Message.IsBodyHtml = true;

                // track email
                body = body + "<img src='" + "https://www.4wdTrip.com.au/track/email.aspx?u=" + userid + "&eid=" + emailqueueid + "' width=0 height=0 />";

                Message.Body = body;

                if (fromuserid > 0)
                {
                    Message.ReplyToList.Add(UserHelper.GetEmail(fromuserid));
                }
                else
                    Message.ReplyToList.Add("team@4wdTrip.com.au");

                
                Message.To.Add(toemail);


                // Init SmtpClient and send
                SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("Houseteam", "88J%a_201");
                smtpClient.Credentials = credentials;
                smtpClient.Send(Message);

                LogHelper.Log("Send By SendGrid: " + toemail);

                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Log("SendGrid Error: " + ex.ToString());
                return false;
            }


        }

        protected void NotifyAdmin(string username, int userid, string email, string emailSubject, string emailBody)
        {
            // add domain to local image (uploaded directly to our site) or they can't view on mobile

            SendEmailRequest request = new SendEmailRequest();

            try
            {

                StringBuilder myString = new StringBuilder();

                myString.Append("Subject:" + emailSubject + "<p></p>");
                myString.Append("Username:" + username + "<p></p>");
                myString.Append("Userid:" + userid + "<p></p>");
                myString.Append("Email:" + email + "<p></p>");
                myString.Append("EmailSubject:" + emailSubject + "<p></p>");
                myString.Append("Body:" + emailSubject + "<p></p>");

                MailMessage Message = new MailMessage();
                Message.From = new MailAddress("team@4wdTrip.com.au", "4WDTRIP.com.au");
                Message.Headers.Add("X-Organization", "4WDTRIP.com.au");
                Message.Subject = "Engine Error " + DateTime.Now.ToString("ddMMyyyyhhmmss");
                Message.Priority = MailPriority.High;
                Message.IsBodyHtml = true;

                Message.Body = myString.ToString();
                Message.To.Add("team@4wdTrip.com.au");

                // Init SmtpClient and send
                SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("Houseteam", "88J%a_201");
                smtpClient.Credentials = credentials;
                smtpClient.Send(Message);

            }
            catch (Exception ex)
            {
                LogHelper.Log(ex.ToString());
            }

        }

        private static Stream GetStreamFromUrl(string url)
        {
            byte[] imageData = null;

            using (var wc = new System.Net.WebClient())
                imageData = wc.DownloadData(url);

            return new MemoryStream(imageData);
        }

        void UploadMap()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = TrackHelper.GetList_Full();
                int trackid = 0;

                TransferUtility fileTransferUtility = new TransferUtility(new AmazonS3Client(Amazon.RegionEndpoint.APSoutheast2));
                string BucketName = System.Configuration.ConfigurationManager.AppSettings["PhotoBucket"];

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    trackid = Convert.ToInt32(dr["TrackID"].ToString());
                    string longtitude = dr["Long"].ToString();
                    string lat = dr["Lat"].ToString();
                    string loc = lat + "," + longtitude;

                    string large = "https://image.maps.cit.api.here.com/mia/1.6/mapview?app_code=vqAt4s_SvFxm43xyvKGwZw&app_id=vCAGPoWfcEhvLb77AEIo&c=" + loc + "&z=15&w=800&h=400";
                    string small = "https://image.maps.cit.api.here.com/mia/1.6/mapview?app_code=vqAt4s_SvFxm43xyvKGwZw&app_id=vCAGPoWfcEhvLb77AEIo&c=" + loc + "&z=15&w=500&h=200";
                    fileTransferUtility.Upload(GetStreamFromUrl(large), BucketName, "tracks/" + trackid + ".jpg");
                    fileTransferUtility.Upload(GetStreamFromUrl(small), BucketName, "tracks/" + trackid + "_sm.jpg");

                    TrackHelper.SetMapImg(trackid, BucketName + "/tracks/" + trackid + ".jpg");

                    LogHelper.Log("Upload Map - " + BucketName + "/tracks/" + trackid + ".jpg");
                }
            }
            catch (Exception ex)
            {
                UpdateStatus(ex.Message);
                LogHelper.Log("Error Upload Map - " + ex.ToString());
            }
            finally
            {
                UpdateStatus("...");
                LogHelper.Log("Finish Upload Map..." + System.DateTime.Now);
            }
        }

        void UpdateStatus(string status)
        {
            this.Invoke((MethodInvoker)delegate
            {
                lblEngineStatus.Text = status;
                lblEngineStatus.Update();
            });

        }

    

    }

}
