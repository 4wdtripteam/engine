﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
namespace tripme
{
    public static class EmailQueueHelper
    {
        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"];

        public static void Add_WithMailoutID(int userid, string subject, string body, string altemail, int fromuserid, int mailoutid, DateTime senddate)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailQueue_Add_WithMailoutID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Subject", subject);
            command.Parameters.AddWithValue("@Body", body);
            command.Parameters.AddWithValue("@AltEmail", altemail);
            command.Parameters.AddWithValue("@FromUserID", fromuserid);
            command.Parameters.AddWithValue("@MailOutID", mailoutid);
            command.Parameters.AddWithValue("@SendDate", senddate);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        public static bool HasSentBefore(string altemail, string emailsubject)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailQueue_HasSentBefore", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@AltEmail", altemail);
            command.Parameters.AddWithValue("@EmailSubject", emailsubject);

            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int CountMailoutUser(int userid, int mailoutid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailQueue_CountMailoutUser", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@mailoutid", mailoutid);

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static void Add(int userid, string subject, string body, string altemail, DateTime senddate)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailQueue_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Subject", subject);
            command.Parameters.AddWithValue("@Body", body);
            command.Parameters.AddWithValue("@AltEmail", altemail);
            command.Parameters.AddWithValue("@FromUserID", 0);
            command.Parameters.AddWithValue("@SendDate", senddate);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static void Add(int userid, string subject, string body, string altemail, int fromuserid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailQueue_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Subject", subject);
            command.Parameters.AddWithValue("@Body", body);
            command.Parameters.AddWithValue("@AltEmail", altemail);
            command.Parameters.AddWithValue("@FromUserID", fromuserid);
            command.Parameters.AddWithValue("@SendDate", DateTime.Now);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static void ResendVerifyEmail(int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailQueue_ResendVerifyEmail", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void ResendAllUserEmail(int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailQueue_ResendAllUserEmail", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void SetOpened(int userid, int emailqueueid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailQueue_SetOpened", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);
            command.Parameters.AddWithValue("@EmailQueueID", emailqueueid);
            command.Parameters.AddWithValue("@OnDate", DateTime.Now);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

    }
}
