﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
using System.Text.RegularExpressions;

using tripme;
namespace tripme
{
    public static class Helper
    {
        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"].ToString();
        public static string sDomain = System.Configuration.ConfigurationManager.AppSettings["Domain"];

        public static double IPAddressToNumber(string IPaddress)
        {
            int i;
            string[] arrDec;
            double num = 0;
            if (IPaddress == "")
            {
                return 0;
            }
            else
            {
                arrDec = IPaddress.Split('.');
                for (i = arrDec.Length - 1; i >= 0; i--)
                {
                    num += ((int.Parse(arrDec[i]) % 256) * Math.Pow(256, (3 - i)));
                }
                return num;
            }
        }

        public static string CleanForumMsg(string original)
        {
            original = System.Text.RegularExpressions.Regex.Replace(original, "(<\\s*[sS][tT][yY][lL][eE].*>)", "<!--$1");
            original = System.Text.RegularExpressions.Regex.Replace(original, "</\\s*[sS][tT][yY][lL][eE]\\s*>", "</style>-->");
            original = original.Replace("<a ", "<a rel=nofollow ").Replace("<A ", "<A rel=nofollow ");
            return original;
        }

        public static void Add(string ip)
        {
            double ips = Helper.IPAddressToNumber(ip);


            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("Test_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@testid", ips);
            command.Parameters.AddWithValue("@IP", ip);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static string FormatWord(string word)
        {
            if (word.Trim() != "")
            {
                string[] longstring;
                string temp;
                string result = "";

                longstring = word.Split(new string[] { "\n", ";", ",", ".", " " }, StringSplitOptions.None);
                foreach (string thisword in longstring)
                {
                    if (Regex.IsMatch(thisword, "^[0-9A-Za-z-']*$") && thisword.Length > 0 && thisword != " ")
                    {
                        temp = thisword.ToLower();
                        result += " " + temp.Substring(0, 1).ToUpper() + temp.Substring(1, temp.Length - 1);
                    }
                }
                return result.Trim();
            }
            else
                return word.Trim();
        }


        public static string AutoURL(string source)
        {
            source = " " + source + " ";
            // easier to convert BR's to something more neutral for now.
            source = Regex.Replace(source, "<br />|<br />|<br/>", "\n");
            source = Regex.Replace(source, @"([\s])(www\..*?|http://.*?)([\s])", "$1<a href=\"$2\" target=\"_blank\">$2</a>$3");
            source = Regex.Replace(source, @"href=""www\.", "href=\"http://www.");
            //source = Regex.Replace(source, "\n", "<br />");
            return source.Trim();
        }

        public static string clearnUrl(string currentUrl, string oldquerystring, string oldqueryvalue, string newquerystring, string newqueryvalue)
        {
            currentUrl = currentUrl.Replace("&" + oldquerystring + "=" + oldqueryvalue, "");
            currentUrl = currentUrl.Replace("?" + oldquerystring + "=" + oldqueryvalue, "?");
            currentUrl = currentUrl.Replace("?&", "?");
            currentUrl = currentUrl.Replace("&&", "&");

            if (currentUrl.Contains("?"))
                currentUrl = currentUrl + "&" + newquerystring + "=" + newqueryvalue;
            else if (currentUrl.Contains("&"))
                currentUrl = currentUrl + "?" + newquerystring + "=" + newqueryvalue;

            return currentUrl;
        }


    }
}
