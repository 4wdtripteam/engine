﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;

namespace tripme
{
    public static class EmailTemplateHelper
    {
        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"];

        /// <summary>
        /// Add Email Template
        /// </summary>
        /// <param name="title"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        public static void Add(string title, string subject, string body)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailTemplate_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            // new template will automatically assign templateid with max id + 1
            command.Parameters.AddWithValue("@Title", title);
            command.Parameters.AddWithValue("@TemplateSubject", subject);
            command.Parameters.AddWithValue("@TemplateBody", body);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        /// <summary>
        /// Edit Email Template
        /// </summary>
        /// <param name="title"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        public static void Set(int templateid, string title, string subject, string body)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailTemplate_Set", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Templateid", templateid);
            command.Parameters.AddWithValue("@Title", title);
            command.Parameters.AddWithValue("@TemplateSubject", subject);
            command.Parameters.AddWithValue("@TemplateBody", body);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        /// <summary>
        /// Get Template Email Subject from tbl_EmailTemplate
        /// </summary>
        /// <param name="templateid"></param>
        /// <returns></returns>
        public static string GetSubject(int templateid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailTemplate_GetSubject", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TemplateID", templateid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        /// <summary>
        /// Get Template Email Body from tbl_EmailTemplate
        /// </summary>
        /// <param name="templateid"></param>
        /// <returns></returns>
        public static string GetBody(int templateid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailTemplate_GetBody", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TemplateID", templateid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        /// <summary>
        /// Get Email Template title (e.g. This email is for verifying email)
        /// </summary>
        /// <param name="templateid"></param>
        /// <returns></returns>
        public static string GetTitle(int templateid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailTemplate_GetTitle", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TemplateID", templateid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet GetAll()
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("EmailTemplate_GetAll", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

    }
}
