﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
namespace tripme
{
    public static class UserDetailHelper
    {
        #region
        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"];

        public static int Add(int userid, int userdetailtypeid, int userdetailvalue, string userdetailtextvalue)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("UserDetail_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@UserDetailTypeID", userdetailtypeid);
            command.Parameters.AddWithValue("@UserDetailValue", userdetailvalue);
            command.Parameters.AddWithValue("@UserDetailTextValue", userdetailtextvalue);

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int Set(int userid, int userdetailtypeid, int userdetailvalue, string userdetailtextvalue)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("UserDetail_Set", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@UserDetailTypeID", userdetailtypeid);
            command.Parameters.AddWithValue("@UserDetailValue", userdetailvalue);
            command.Parameters.AddWithValue("@UserDetailTextValue", userdetailtextvalue);

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        public static int GetByUserDetailTypeID(int userid, int userdetailtypeid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("UserDetail_GetByUserDetailTypeID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@UserDetailTypeID", userdetailtypeid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static string GetTextByUserDetailTypeID(int userid, int userdetailtypeid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("UserDetail_GetTextByUserDetailTypeID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@UserDetailTypeID", userdetailtypeid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static void Delete(int userid, int userdetailtypeid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("UserDetail_Delete", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@UserDetailTypeID", userdetailtypeid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static bool Has(int userid, int userdetailtypeid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("UserDetail_Has", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@UserDetailTypeID", userdetailtypeid);
            try
            {
                objConnection.Open();
                return Convert.ToBoolean(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }

        public static int GetUserIDByUserDetail(int userdetailtypeid, int userdetailvalue)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("UserDetail_GetUserIDByUserDetail", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserDetailTypeID", userdetailtypeid);
            command.Parameters.AddWithValue("@UserDetailValue", userdetailvalue);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int GetUserIDByUserDetailText(int userdetailtypeid, string userdetailtextvalue)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("UserDetail_GetUserIDByUserDetailText", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserDetailTypeID", userdetailtypeid);
            command.Parameters.AddWithValue("@UserDetailTextValue", userdetailtextvalue);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        #endregion
    }
}
