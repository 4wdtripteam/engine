﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
using System.Web.Security;
using System.Web.Configuration;

namespace tripme
{
    public static class TripUserHelper
    {
        #region
        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"];
        public static string sDomain = System.Configuration.ConfigurationManager.AppSettings["Domain"];
        
        
        public static int Add(int tripid, int userid, int msgid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripUser_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@TripID", tripid);
            command.Parameters.AddWithValue("@OnDate", DateTime.Now);
            command.Parameters.AddWithValue("@MsgID", msgid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static int CountByStatus(int tripid, int status)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripUser_CountByStatus", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripID", tripid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        public static bool Has(int tripid, int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripUser_Has", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripID", tripid);
            command.Parameters.AddWithValue("@UserID", userid);

            try
            {
                objConnection.Open();
                return Convert.ToBoolean(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }




        public static DataSet GetByTripID(int tripid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripUser_GetByTripID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripID", tripid);
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int GetStatus(int tripid, int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripUser_GetStatus", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripID", tripid);
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int Count(int tripid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripUser_Count", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripID", tripid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static int CountByUserID(int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripUser_CountByUserID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static void SetStatus(int tripid, int userid, int status)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripUser_SetStatus", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripID", tripid);
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void Delete(int tripid, int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripUser_Delete", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripID", tripid);
            command.Parameters.AddWithValue("@UserID", userid);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        #endregion

    }
}
