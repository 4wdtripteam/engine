﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
using System.Web.Security;
using System.Web.Configuration;


namespace tripme
{
    public static class UserHelper
    {

        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"].ToString();
        public static string sDomain = System.Configuration.ConfigurationManager.AppSettings["Domain"];

        public static int Add(string username, string firstname, string lastname, string email, string address, string city, string postcode, string state, string country, string pwd, string ip,
            int usertype, int gender, string age, int accounttype, string company)
        {

            pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "SHA1");

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Username", username);
            command.Parameters.AddWithValue("@Firstname", firstname);
            command.Parameters.AddWithValue("@Lastname", lastname);
            command.Parameters.AddWithValue("@Email", email);
            command.Parameters.AddWithValue("@Address", address);
            command.Parameters.AddWithValue("@City", city);
            command.Parameters.AddWithValue("@Postcode", postcode);
            command.Parameters.AddWithValue("@State", state);
            command.Parameters.AddWithValue("@Country", country);
            command.Parameters.AddWithValue("@Pwd", pwd);
            command.Parameters.AddWithValue("@IP", ip);
            command.Parameters.AddWithValue("@UserType", usertype);
            command.Parameters.AddWithValue("@Gender", gender);
            command.Parameters.AddWithValue("@Age", age);
            command.Parameters.AddWithValue("@AccountType", accounttype);
            command.Parameters.AddWithValue("@Company", company);

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet NotifyAllUser()
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_NotifyAllUser", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static DataSet NotifyAllUser_ByState(string state)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_NotifyAllUser_ByState", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@State", state);

            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        /// <summary>
        /// Get User's hash password
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static string GetPwd(int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetPwd", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);

            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static string GetCompany(int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetCompany", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);

            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void SetProfilePic(int userid, bool profilepic)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_SetProfilePic", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);
            command.Parameters.AddWithValue("@ProfilePic", profilepic);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        /// <summary>
        /// Set User password
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="clearpwd"></param>
        public static void SetPwd(int userid, string pwd)
        {
            pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "SHA1");

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_SetPwd", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);
            command.Parameters.AddWithValue("@Pwd", pwd);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void UpdateUpload(int userid, int upload)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_UpdateUpload", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);
            command.Parameters.AddWithValue("@Upload", upload);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void UpdatePt(int userid, int pt)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_UpdatePt", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);
            command.Parameters.AddWithValue("@Pt", pt);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void SetPtLevel(int userid, int ptlevel)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_SetPtLevel", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);
            command.Parameters.AddWithValue("@PtLevel", ptlevel);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void SetUserType(int userid, int usertype)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_SetUserType", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);
            command.Parameters.AddWithValue("@UserType", usertype);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static void SetEmail(int userid, string email)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_SetEmail", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);
            command.Parameters.AddWithValue("@Email", email);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        /// <summary>
        /// Check if match user's password
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public static bool IsPwdMatch(int userid, string pwd)
        {

            pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "SHA1");

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_IsPwdMatch", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);
            command.Parameters.AddWithValue("@Pwd", pwd);

            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        /// <summary>
        /// Check if the username already existed
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public static bool HasUsername(int userid, string username)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_HasUsername", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);
            command.Parameters.AddWithValue("@Username", username);

            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        /// <summary>
        /// Check if email already existed
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool HasEmail(int userid, string email)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_HasEmail", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);
            command.Parameters.AddWithValue("@Email", email);

            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        /// <summary>
        /// Save User's profile
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="username"></param>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        /// <param name="bizname"></param>
        /// <param name="country"></param>
        public static void SaveProfile(int userid, string username, string firstname, string lastname, string email,
            string address, string city, string postcode, string state, string country, string age, int gender, string company)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_SaveProfile", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Username", username);
            command.Parameters.AddWithValue("@Firstname", firstname);
            command.Parameters.AddWithValue("@Lastname", lastname);
            command.Parameters.AddWithValue("@Email", email);
            command.Parameters.AddWithValue("@Address", address);
            command.Parameters.AddWithValue("@City", city);
            command.Parameters.AddWithValue("@Postcode", postcode);
            command.Parameters.AddWithValue("@State", state);
            command.Parameters.AddWithValue("@Country", country);
            command.Parameters.AddWithValue("@Age", age);
            command.Parameters.AddWithValue("@Gender", gender);
            command.Parameters.AddWithValue("@Company", company);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        public static void SetLocation(int userid, string address, string city, string postcode, string state, string country)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_SetLocation", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Address", address);
            command.Parameters.AddWithValue("@City", city);
            command.Parameters.AddWithValue("@Postcode", postcode);
            command.Parameters.AddWithValue("@State", state);
            command.Parameters.AddWithValue("@Country", country);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }



        /// <summary>
        /// Return User's profile in My Profile page
        /// </summary>
        /// <returns></returns>
        public static DataSet GetProfile(int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetProfile", objConnection);
            command.Parameters.AddWithValue("@Userid", userid);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "user");

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet GetPending(int usertype, int userdetailtypeid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetPending", objConnection);
            command.Parameters.AddWithValue("@UserType", usertype);
            command.Parameters.AddWithValue("@UserDetailTypeID", userdetailtypeid);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "user");

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        public static DataSet GetTopCooks(int rpp)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetTopCooks", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Rpp", rpp);

            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "user");

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static int GetGender(int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetGender", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        /// <summary>
        /// Get UserID by Username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static int GetUserIDByUsername(string username)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetUserIDByUsername", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Username", username);

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int GetUserIDByEmail(string email)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetUserIDByEmail", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Email", email);

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int GetMaxUserID()
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetMaxUserID", objConnection);
            command.CommandType = CommandType.StoredProcedure;

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static string GetUsernameByUserID(int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetUsernameByUserID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);

            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        public static string GetFullname(int userid)
        {
            return GetFirstname(userid) + " " + GetLastname(userid);
        }
        public static string GetFirstname(int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetFirstname", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);

            try
            {
                objConnection.Open();
                string firstname = command.ExecuteScalar().ToString();
                if (!firstname.Contains("-"))
                    firstname = Helper.FormatWord(firstname);
                return firstname;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static DataSet TripAlert(int userdetailtypeid, string userdetailvalue, int alertype)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_TripAlert", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserDetailTypeID", userdetailtypeid);
            command.Parameters.AddWithValue("@UserDetailValue", userdetailvalue);
            command.Parameters.AddWithValue("@AlertType", alertype);

            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static string GetLastname(int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetLastname", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);

            try
            {
                objConnection.Open();
                if (command.ExecuteScalar().ToString() == null || command.ExecuteScalar().ToString() == "")
                    return "";

                string lastname = command.ExecuteScalar().ToString();
                if (lastname != null && lastname != "")
                {
                    lastname = Helper.FormatWord(lastname);
                }
                else
                {
                    return "";
                }
                return lastname;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static string GetAge(int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetAge", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);

            try
            {
                objConnection.Open();
                string lastname = command.ExecuteScalar().ToString();
                lastname = Helper.FormatWord(lastname);
                return lastname;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        /// <summary>
        /// Set User's status
        /// </summary>
        /// <param name="userid"></param>
        public static void SetStatus(int userid, int status)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_SetStatus", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Status", status);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static void SetCountry(int userid, string country)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_SetCountry", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Country", country);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void SetCompany(int userid, string company)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_SetCompany", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Company", company);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void Cancel(int userid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_Cancel", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Userid", userid);

            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }



        public static DataSet GetHouseSitters2(string sortby, int currentpage,
            string gender,
            string userdetailtypeid1, string userdetailvalue1,
            string userdetailtypeid2, string userdetailvalue2,
            string userdetailtypeid3, string userdetailvalue3,
            string userdetailtypeid4, string userdetailvalue4,
            string userdetailtypeid5, string userdetailvalue5,
            string userdetailtypeid6, string userdetailvalue6,
            string userdetailtypeid7, string userdetailvalue7,
            string userdetailtypeid8, string userdetailvalue8


            )
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetHouseSitters2", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SortBy", sortby);
            command.Parameters.AddWithValue("@CurrentPage", currentpage);
            if (gender != "")
                command.Parameters.AddWithValue("@Gender", gender);
            if (userdetailtypeid1 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID1", userdetailtypeid1);
            if (userdetailvalue1 != "")
                command.Parameters.AddWithValue("@UserDetailValue1", userdetailvalue1);
            if (userdetailtypeid2 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID2", userdetailtypeid2);
            if (userdetailvalue2 != "")
                command.Parameters.AddWithValue("@UserDetailValue2", userdetailvalue2);
            if (userdetailtypeid3 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID3", userdetailtypeid3);
            if (userdetailvalue3 != "")
                command.Parameters.AddWithValue("@UserDetailValue3", userdetailvalue3);
            if (userdetailtypeid4 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID4", userdetailtypeid4);
            if (userdetailvalue4 != "")
                command.Parameters.AddWithValue("@UserDetailValue4", userdetailvalue4);
            if (userdetailtypeid5 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID5", userdetailtypeid5);
            if (userdetailvalue5 != "")
                command.Parameters.AddWithValue("@UserDetailValue5", userdetailvalue5);
            if (userdetailtypeid6 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID6", userdetailtypeid6);
            if (userdetailvalue6 != "")
                command.Parameters.AddWithValue("@UserDetailValue6", userdetailvalue6);
            if (userdetailtypeid7 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID7", userdetailtypeid7);
            if (userdetailvalue7 != "")
                command.Parameters.AddWithValue("@UserDetailValue7", userdetailvalue7);
            if (userdetailtypeid8 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID8", userdetailtypeid8);
            if (userdetailvalue8 != "")
                command.Parameters.AddWithValue("@UserDetailValue8", userdetailvalue8);

            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet GetHouseSitters(string sortby, int currentpage, int userdetailtypeid, string userdetailvalue)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetHouseSitters", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SortBy", sortby);
            command.Parameters.AddWithValue("@CurrentPage", currentpage);
            command.Parameters.AddWithValue("@UserDetailTypeID", userdetailtypeid);
            command.Parameters.AddWithValue("@UserDetailValue", userdetailvalue);
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static DataSet GetHouseSitters_All(string sortby, int currentpage)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetHouseSitters_All", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SortBy", sortby);
            command.Parameters.AddWithValue("@CurrentPage", currentpage);
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet GetSitterByPreferedLocation(int userdetailtypeid, string userdetailvalue)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetSitterByPreferedLocation", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserDetailTypeID", userdetailtypeid);
            command.Parameters.AddWithValue("@UserDetailValue", userdetailvalue);
            command.Parameters.AddWithValue("@Now", DateTime.Now);
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet GetFeaturedHouseSitter(string state)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetFeaturedHouseSitter", objConnection);
            command.Parameters.AddWithValue("@Now", DateTime.Now);
            if (state != "")
                command.Parameters.AddWithValue("@State", state);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "user");

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static DataSet GetLatestHouseSitter()
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetLatestHouseSitter", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "user");

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet GetUnverified(int usertype)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetUnverified", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserType", usertype);
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        public static DataSet GetUserID_Sitemap(int usertype)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetUserID_Sitemap", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserType", usertype);
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        public static DataSet Get(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_Get", objConnection);
            command.Parameters.AddWithValue("@UserID", userid);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "user");

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static DataSet GetByUsername(string username)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetByUsername", objConnection);
            command.Parameters.AddWithValue("@Username", username);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "user");

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static DataSet GetByEmail(string email)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetByEmail", objConnection);
            command.Parameters.AddWithValue("@Email", email);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "user");

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet Search(string firstname, string lastname)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_Search", objConnection);
            if (firstname != "")
                command.Parameters.AddWithValue("@Firstname", firstname);
            if (lastname != "")
                command.Parameters.AddWithValue("@Lastname", lastname);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "user");

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static bool IsUsernameExist(string username)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_IsUsernameExist", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Username", username);

            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static bool IsUserIDExist(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_IsUserIDExist", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);

            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static bool IsEmailExist(string email)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_IsEmailExist", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Email", email);

            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static bool IsVerify(string email, int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_IsVerify", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Email", email);
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int GetUserType(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetUserType", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int GetStatus2(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetStatus", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static int GetStatus(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetStatus", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static string GetEmail(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetEmail", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static string GetPostcode(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetPostcode", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static string GetCity(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetCity", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static string GetState(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetState", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static string GetApproxAddress(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetApproxAddress", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        public static string GetCountryName(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetCountryName", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static string GetFullAddressText(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetFullAddressText", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static int GetLatestUserID()
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetLatestUserID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static int GetPt(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetPt", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int CountHouseSitters(String state)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_CountHouseSitters", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            if (state != "")
                command.Parameters.AddWithValue("@State", state);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int CountHouseSitters2(
            string gender,
            string userdetailtypeid1, string userdetailvalue1,
            string userdetailtypeid2, string userdetailvalue2,
            string userdetailtypeid3, string userdetailvalue3,
            string userdetailtypeid4, string userdetailvalue4,
            string userdetailtypeid5, string userdetailvalue5,
            string userdetailtypeid6, string userdetailvalue6,
            string userdetailtypeid7, string userdetailvalue7,
            string userdetailtypeid8, string userdetailvalue8


            )
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_CountHouseSitters2", objConnection);
            command.CommandType = CommandType.StoredProcedure;

            if (gender != "")
                command.Parameters.AddWithValue("@Gender", gender);
            if (userdetailtypeid1 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID1", userdetailtypeid1);
            if (userdetailvalue1 != "")
                command.Parameters.AddWithValue("@UserDetailValue1", userdetailvalue1);
            if (userdetailtypeid2 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID2", userdetailtypeid2);
            if (userdetailvalue2 != "")
                command.Parameters.AddWithValue("@UserDetailValue2", userdetailvalue2);
            if (userdetailtypeid3 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID3", userdetailtypeid3);
            if (userdetailvalue3 != "")
                command.Parameters.AddWithValue("@UserDetailValue3", userdetailvalue3);
            if (userdetailtypeid4 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID4", userdetailtypeid4);
            if (userdetailvalue4 != "")
                command.Parameters.AddWithValue("@UserDetailValue4", userdetailvalue4);
            if (userdetailtypeid5 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID5", userdetailtypeid5);
            if (userdetailvalue5 != "")
                command.Parameters.AddWithValue("@UserDetailValue5", userdetailvalue5);
            if (userdetailtypeid6 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID6", userdetailtypeid6);
            if (userdetailvalue6 != "")
                command.Parameters.AddWithValue("@UserDetailValue6", userdetailvalue6);
            if (userdetailtypeid7 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID7", userdetailtypeid7);
            if (userdetailvalue7 != "")
                command.Parameters.AddWithValue("@UserDetailValue7", userdetailvalue7);
            if (userdetailtypeid8 != "")
                command.Parameters.AddWithValue("@UserDetailTypeID8", userdetailtypeid8);
            if (userdetailvalue8 != "")
                command.Parameters.AddWithValue("@UserDetailValue8", userdetailvalue8);

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static int CountHouseSitters_All(String state)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_CountHouseSitters_All", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            if (state != "")
                command.Parameters.AddWithValue("@State", state);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int CountHouseSitters_ByPreferedLocation(int userdetailtypeid, string userdetailvalue)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_CountHouseSitters_ByPreferedLocation", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserDetailTypeID", userdetailtypeid);
            command.Parameters.AddWithValue("@UserDetailValue", userdetailvalue);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static int GetPtLevel(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetPtLevel", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static bool HasProfilePic(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_HasProfilePic", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int GetProfilePic(int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("User_GetProfilePic", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static string GetSignature(int userid, int style, bool withPhoto, int photosize, int fixwidth, int fixheight)
        {


            string username = "";
            if (style == 1)
            {
                username = "<strong>" + GetUsernameByUserID(userid) + "</strong>";
                if (withPhoto)
                {
                    username = GetProfilePhotoLink(userid, photosize, false, fixwidth, fixheight) + " " + username;
                }
            }
            else if (style == 2)
            {
                username = "<a href='" + sDomain + "/member/" + userid + "'>" + GetUsernameByUserID(userid) + "</a>";
                if (withPhoto)
                {
                    username = "<a href='" + sDomain + "/member/" + userid + "' >" + GetProfilePhotoLink(userid, photosize, false, fixwidth, fixheight) + "</a>" + " " + username;
                }
            }
            else if (style == 3)
            {
                username = "<span class='mediumfont'><a href='" + sDomain + "/member/" + userid + "'>" + GetUsernameByUserID(userid) + "</a></span>";
                if (withPhoto)
                {
                    username = "<a href='" + sDomain + "/member/" + userid + "' >" + GetProfilePhotoLink(userid, photosize, false, fixwidth, fixheight) + "</a>" + " " + username;
                }
            }
            else { }

            int pt = GetPt(userid);
            int ptlevel = GetPtLevel(userid);
            string level = "(" + pt + " <a href='" + sDomain + "/help/read.aspx?helpid=13' target='_blank' ><img src='" + sDomain + "/img/badge/" + ptlevel + ".gif' alt='Dish Level " + ptlevel + "' title='Dish Level " + ptlevel + "' border='0' /></a>)";

            string signature = username + " " + level;

            return signature;

        }

        public static string GetProfilePhotoLink(int userid, int size, bool allowcache, int fixwidth, int fixheight)
        {

            string width = "";
            string height = "";

            if (fixwidth > 0 && fixheight > 0)
            {
                width = " style='width:" + fixwidth.ToString() + "px' ";
            }
            else if (fixwidth > 0 && fixheight == 0)
                width = " style='width:" + fixwidth.ToString() + "px' ";
            else if (fixheight > 0 && fixwidth == 0)
                height = " style='height:" + fixheight.ToString() + "px' ";


            string photo = "";
            if (UserHelper.HasProfilePic(userid))
            {
                string firstname = UserHelper.GetFirstname(userid);

                string ran = DateTime.Now.Millisecond.ToString();
                if (allowcache)
                    ran = "";

                string piclocPath = PicLocHelper.GetPicLocPath(0);

                photo = " <img src='" + piclocPath + "/" + userid + "/" + userid + "s" + size + ".jpg?" + ran + "' alt='" + firstname + "' title='" + firstname + "'  border='0' " + width + height + " />";
            }
            else if (UserDetailHelper.Has(userid, 14))
            {
                string facebookid = UserDetailHelper.GetTextByUserDetailTypeID(userid, 14);
                photo = "<img src='https://graph.facebook.com/" + facebookid + "/picture?type=normal'" + width + height + " />";
            }

            else
                photo = " <img src='http://photo.4wdtrip.com.au/blank_sm.gif' border='0' " + width + height + " />";

            return photo;
        }
        public static string GetPtLevelIcon(int ptlevel)
        {
            return " <img src='" + sDomain + "/img/badge/" + ptlevel + ".gif' alt='Dish Level " + ptlevel + "' title='Dish Level " + ptlevel + "' border='0' />";

        }



    }
}
