﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
namespace tripme
{
    public static class TripDetailHelper
    {
        #region
        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"];

        public static int Add(int tripid, int tripdetailtypeid, int tripdetailvalue, string tripdetailtextvalue)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripDetail_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripID", tripid);
            command.Parameters.AddWithValue("@TripDetailTypeID", tripdetailtypeid);
            command.Parameters.AddWithValue("@TripDetailValue", tripdetailvalue);
            command.Parameters.AddWithValue("@TripDetailTextValue", tripdetailtextvalue);

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int Set(int tripid, int tripdetailtypeid, int tripdetailvalue, string tripdetailtextvalue)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripDetail_Set", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripID", tripid);
            command.Parameters.AddWithValue("@TripDetailTypeID", tripdetailtypeid);
            command.Parameters.AddWithValue("@TripDetailValue", tripdetailvalue);
            command.Parameters.AddWithValue("@TripDetailTextValue", tripdetailtextvalue);

            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }


        public static int GetByTripDetailTypeID(int tripid, int tripdetailtypeid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripDetail_GetByTripDetailTypeID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripID", tripid);
            command.Parameters.AddWithValue("@TripDetailTypeID", tripdetailtypeid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static string GetTextByTripDetailTypeID(int tripid, int tripdetailtypeid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripDetail_GetTextByTripDetailTypeID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripID", tripid);
            command.Parameters.AddWithValue("@TripDetailTypeID", tripdetailtypeid);
            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static void Delete(int tripid, int tripdetailtypeid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripDetail_Delete", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripID", tripid);
            command.Parameters.AddWithValue("@TripDetailTypeID", tripdetailtypeid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static bool Has(int tripid, int tripdetailtypeid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripDetail_Has", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripID", tripid);
            command.Parameters.AddWithValue("@TripDetailTypeID", tripdetailtypeid);
            try
            {
                objConnection.Open();
                return Convert.ToBoolean(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }

        public static int GetTripIDByTripDetail(int tripdetailtypeid, int tripdetailvalue)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripDetail_GetTripIDByTripDetail", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripDetailTypeID", tripdetailtypeid);
            command.Parameters.AddWithValue("@TripDetailValue", tripdetailvalue);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int GetTripIDByTripDetailText(int tripdetailtypeid, string tripdetailtextvalue)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("TripDetail_GetTripIDByTripDetailText", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@TripDetailTypeID", tripdetailtypeid);
            command.Parameters.AddWithValue("@TripDetailTextValue", tripdetailtextvalue);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        #endregion
    }
}
