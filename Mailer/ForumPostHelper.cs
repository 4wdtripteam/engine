﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
namespace tripme
{
    public static class ForumPostHelper
    {
        #region
        public static string MyConnection = System.Configuration.ConfigurationManager.AppSettings["ConnectDB"];

        public static int Add(int forumid, int replyforumpostid, int replytoid, int userid, string title,  string message,
            DateTime ondate, bool istitlepost, bool islocked, bool isallowreply, bool isadmin,
            int viewed, int likeit, int replies, int lastreplyforumpostid, DateTime lastreplypostdate, int lastreplyuserid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_Add", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumID", forumid);
            command.Parameters.AddWithValue("@ReplyForumPostID", replyforumpostid);
            command.Parameters.AddWithValue("@ReplyToID", replytoid);
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Title", title);
            command.Parameters.AddWithValue("@Message", message);
            command.Parameters.AddWithValue("@OnDate", ondate);
            command.Parameters.AddWithValue("@IsTitlePost", istitlepost);
            command.Parameters.AddWithValue("@IsLocked", islocked);
            command.Parameters.AddWithValue("@IsAllowReply", isallowreply);
            command.Parameters.AddWithValue("@IsAdmin", isadmin);
            command.Parameters.AddWithValue("@Viewed", viewed);
            command.Parameters.AddWithValue("@Likeit", likeit);
            command.Parameters.AddWithValue("@Replies", replies);
            command.Parameters.AddWithValue("@LastReplyForumPostID", lastreplyforumpostid);
            command.Parameters.AddWithValue("@LastReplyPostDate", lastreplypostdate);
            command.Parameters.AddWithValue("@LastReplyUserID", lastreplyuserid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void Edit(int forumpostid, int userid, string title,  string message)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_Edit", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            command.Parameters.AddWithValue("@UserID", userid);
            command.Parameters.AddWithValue("@Title", title);
            command.Parameters.AddWithValue("@Message", message);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void Update(int forumpostid, int replies, int lastreplyforumpostid, DateTime lastreplypostdate, int lastreplyuserid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_Update", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            command.Parameters.AddWithValue("@Replies", replies);
            command.Parameters.AddWithValue("@LastReplyForumPostID", lastreplyforumpostid);
            command.Parameters.AddWithValue("@LastReplyPostDate", lastreplypostdate);
            command.Parameters.AddWithValue("@LastReplyUserID", lastreplyuserid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void SetStatus(int forumpostid, int userid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_SetStatus", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            command.Parameters.AddWithValue("@Status", status);
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void SetStatusByReplyForumPostID(int replyforumpostid, int userid, int status)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_SetStatusByReplyForumPostID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ReplyForumPostID", replyforumpostid);
            command.Parameters.AddWithValue("@Status", status);
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static DataSet BrowsePost(int forumpostid, int rpp, int currentpage, string search)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_BrowsePost", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            command.Parameters.AddWithValue("@Rpp", rpp);
            command.Parameters.AddWithValue("@CurrentPage", currentpage);
            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static DataSet BrowseTitle(int forumid, int rpp, int currentpage, string search, string sortby)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_BrowseTitle", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumID", forumid);
            command.Parameters.AddWithValue("@Rpp", rpp);
            command.Parameters.AddWithValue("@CurrentPage", currentpage);
            command.Parameters.AddWithValue("@SortBy", sortby);

            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static DataSet BrowseTitleOnly(int forumid, int rpp)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_BrowseTitleOnly", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumID", forumid);
            command.Parameters.AddWithValue("@Rpp", rpp);

            try
            {
                objConnection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "record");
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
       
        
        public static string GetTitle(int forumpostid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_GetTitle", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);

            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static string GetMessage(int forumpostid)
        {

            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_GetMessage", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);

            try
            {
                objConnection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static int GetUserID(int forumpostid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_GetUserID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
        public static int GetForumID(int forumpostid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_GetForumID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
        public static int GetStatus(int forumpostid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_GetStatus", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }

        public static int GetReplyForumPostID(int forumid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_GetReplyForumPostID", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }

        public static void Delete(int forumpostid, int userid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_Delete", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            command.Parameters.AddWithValue("@UserID", userid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void IncreViewed(int forumpostid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_IncreViewed", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }
        public static void IncreLikeit(int forumpostid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_IncreLikeit", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            try
            {
                objConnection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }
        }

        public static bool GetIsTitlePost (int forumpostid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_GetIsTitlePost", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
        public static bool GetIsLocked(int forumpostid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_GetIsLocked", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
        public static bool GetIsAdmin(int forumpostid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_GetIsAdmin", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
        public static bool GetViewed(int forumpostid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_GetViewed", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
        public static bool GetLikeit(int forumpostid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_GetLikeit", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            try
            {
                objConnection.Open();
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }

        public static int CountTitle(int forumid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_CountTitle", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumID", forumid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
        public static int CountPost(int forumpostid)
        {
            SqlConnection objConnection = new SqlConnection(MyConnection);
            SqlCommand command = new SqlCommand("ForumPost_CountPost", objConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ForumPostID", forumpostid);
            try
            {
                objConnection.Open();
                return Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                objConnection.Close();
            }

        }
    
        #endregion

    }
}
