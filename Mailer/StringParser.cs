﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
namespace tripmemeBL
{
    public class StringParser
    {
        List<string> alPos = new List<string>();
        List<string> alNeg = new List<string>();
        List<string> alSort;
        bool bNeg;

        public bool NegativeSP()
        {
            return bNeg;
        }

        public StringParser(List<string> alPositive, List<string> alNegative)
        {
            alPos = alPositive;
            alNeg = alNegative;
            bNeg = false;
        }

        public string Parsing()
        {
            int i = 0;
            int j = 0;
            string sBlock = "";
            string sOut = "";
            int iCountPos = alPos.Count;
            int iCountNeg = alNeg.Count;

            alSort = new List<string>();

            if (iCountPos > 0)
            {
                for (i=0; i <= alPos.Count - 1; i++)
                {
                    sBlock = alPos[i] + " ";
                    for (j = 0; j <= alNeg.Count - 1; j++)
                    {
                        sBlock += alNeg[j] + " ";
                    }
                    alSort.Add(sBlock);
                }
                sOut = KeywordConversion();
            }
            else
            {
                if (iCountNeg > 0)
                {
                    alSort = alNeg;
                    sOut = NegativeConversion();
                }
                else
                    sOut = "";
            }

            return sOut;

        }

        private string NegativeConversion()
        {
            char c = (char)32;
            Char[] csplit = {c};
            string[] aWord;
            int i, j;
            string sNormal = "";
            string sForms = "";
            string sWord = "";
            string sOut = "";

            for (j = 0; j <= alSort.Count - 1; j++)
            {
                aWord = alSort[j].ToString().Replace("-", " ").Split(csplit, StringSplitOptions.RemoveEmptyEntries);
                sNormal = "";
                sForms = "";
                for (i = 0; i <= aWord.Length - 1; i++)
                {
                    if(!IsSQLKey(aWord[i]))
                    {
                        sWord = (char)34 + aWord[i] + (char)34;
                        if(aWord[i].Trim().EndsWith("*"))
                        {
                            sNormal += " OR " + sWord;
                        }
                        else{
                            sNormal += " OR " + sWord;
                            sNormal += " OR FORMSOF(INFLECTIONAL," + aWord + ")";
                        }
                    }
                }

                if(sNormal.Trim().StartsWith("OR"))
                    sNormal = sNormal.Trim().Substring(2);

                if(sForms.Trim().StartsWith("OR"))
                    sForms = sForms.Trim().Substring(2);

                if(sNormal!="" && sForms!="")
                    sOut += " (( " + sNormal + " ) OR ( " + sForms + " )) OR";
                else if(sNormal!="" && sForms!="")
                    sOut += " ( " + sNormal + " ) OR";

            }
            if(sOut.Length > 2)
                sOut = sOut.Substring(0, sOut.Length - 2);

            sOut = sOut.Replace("_", " ");
            bNeg = true;
            return sOut;

        }

        public string KeywordConversion()
        {
            char c = (char)32;
            Char[] csplit = { c };
            string[] aWord;
            int i, j;
            string sNormal = "";
            string sForms = "";
            string sWord = "";
            string sOut = "";
            for (j = 0; j <= alSort.Count - 1; j++)
            {
                aWord = alSort[j].ToString().Split(csplit, StringSplitOptions.RemoveEmptyEntries);
                sNormal = "";
                sForms = "";
                for (i = 0; i <= aWord.Length - 1; i++)
                {
                    if(!IsSQLKey(aWord[i]))
                    {
                        if(aWord[i].Trim().StartsWith("-") && !aWord[i].Trim().EndsWith("*"))
                        {
                            sWord = (char)34 + aWord[i].Substring(1) + (char)34;
                            sNormal += " AND NOT " + sWord;
                            sForms += " AND NOT FORMSOF(INFLECTIONAL," + sWord + ")";
                        }
                        else if(aWord[i].Trim().EndsWith("*") && !aWord[i].Trim().StartsWith("-"))
                        {
                            sWord = (char)34 + aWord[i] + (char)34;
                            sNormal += " AND " + sWord;
                            sForms += " AND " + sWord;
                        }
                        else if (aWord[i].Trim().StartsWith("-") && aWord[i].Trim().EndsWith("*"))
                        {
                            sWord = (char)34 + aWord[i].Substring(1) + (char)34;
                            sNormal += " AND NOT " + sWord;
                            sForms += " AND NOT " + sWord;
                        }
                        else
                        {
                            sWord = (char)34 + aWord[i] + (char)34;
                            sNormal += " AND " + sWord;
                            sForms += " AND FORMSOF(INFLECTIONAL," + sWord + ")";
                        }
                        
                    }
                }

                if (sNormal.Trim().StartsWith("AND"))
                    sNormal = sNormal.Trim().Substring(3);

                if (sForms.Trim().StartsWith("AND"))
                    sForms = sForms.Trim().Substring(3);

                if (sNormal != "")
                {
                    sOut += " (( " + sNormal + " ) OR ( " + sForms + " )) OR";
                }

            }

            if (sOut.Length > 2)
                sOut = sOut.Substring(0, sOut.Length - 2);

            sOut = sOut.Replace("_", " ");
            return sOut;
        }
        private bool IsSQLKey(string sIn)
        {
            sIn = Regex.Replace(sIn.Trim().ToLower(), "\\-\\*","");
            switch(sIn)
            {
                case "of":
                    return true;
                case "and":
                    return true;
                case "or":
                    return true;
                case "at":
                    return true;
                case "a":
                    return true;
                case "an":
                    return true;
                case "the":
                    return true;
                case "it":
                    return true;
                case "for":
                    return true;
                default:
                    return false;


            }
        }
    }
}
